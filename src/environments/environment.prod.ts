export const environment = {
  production: true,
  apiPrefix: 'https://africarider.net/arul/drupp-api/api/admin/',
  assetsUrl: 'https://africarider.net/arul/drupp-api/storage/app/public',
  currency: '&#8358; ',
  authKey: 'drupp_token',
  adminId: 'admin_id',
  firebase: {
    apiKey: 'AIzaSyDOcDbk1PO_qgVJdLDkmNJBF6LAnugEwBM',
    authDomain: 'drupp-app.firebaseapp.com',
    databaseURL: 'https://drupp-app.firebaseio.com',
    projectId: 'drupp-app',
    storageBucket: 'drupp-app.appspot.com',
    messagingSenderId: '673734351855',
    appId: '1:673734351855:web:fcbc7b2faec26a68'
  }
};
