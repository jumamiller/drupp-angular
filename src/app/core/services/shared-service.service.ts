import {EventEmitter, Injectable} from '@angular/core';

@Injectable()
export class SharedServiceService {
  userLoggedIn = new EventEmitter<any>();
  userLoggedOut = new EventEmitter<any>();
  startChat = new EventEmitter<any>();
  openExistingChatBox = new EventEmitter<any>();
  pushMessageExistingChat = new EventEmitter<any>();

  constructor() {
  }
}
