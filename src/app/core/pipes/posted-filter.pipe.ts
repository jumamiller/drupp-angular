import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'postedFilter'
})
export class PostedFilterPipe implements PipeTransform {
  transform(ride, rideSearch: string) {
    if (ride && ride.length) {
      return ride.filter(item => {
        if (rideSearch && item.source.toLowerCase().indexOf(rideSearch.toLowerCase()) === -1
          && rideSearch && item.destination.toLowerCase().indexOf(rideSearch.toLowerCase()) === -1
          && rideSearch && item.total_fare.toLowerCase().indexOf(rideSearch.toLowerCase()) === -1) {
          return false;
        }
        return true;
      });
    } else {
      return ride;
    }
  }

}
