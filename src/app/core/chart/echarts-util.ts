export function echartDoughnut(
    // series_name:string,
    // series_data: number[],
    // graph_height: number,
    // graph_type: string,
    // graph_title: string,
    // x_axis_categories: string[],
    // curve_stroke: string
)
{
    return {
        tooltip: {
            trigger: 'item'
        },
        legend: {
            top: '5%',
            left: 'center'
        },
        series: [
            {
                name: 'Rides',
                type: 'pie',
                radius: ['40%', '70%'],
                avoidLabelOverlap: false,
                label: {
                    show: false,
                    position: 'center'
                },
                emphasis: {
                    label: {
                        show: false,
                        fontSize: '40',
                        fontWeight: 'bold'
                    }
                },
                labelLine: {
                    show: false
                },
                data: [
                    {value: 1048, name: 'Completed Rides'},
                    {value: 735, name: 'Total Rides'},
                ]
            }
        ]
    };
}
