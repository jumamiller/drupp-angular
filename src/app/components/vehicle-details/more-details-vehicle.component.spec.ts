import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoreDetailsVehicleComponent } from './more-details-vehicle.component';

describe('MoreDetailsVehicleComponent', () => {
  let component: MoreDetailsVehicleComponent;
  let fixture: ComponentFixture<MoreDetailsVehicleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoreDetailsVehicleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoreDetailsVehicleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
