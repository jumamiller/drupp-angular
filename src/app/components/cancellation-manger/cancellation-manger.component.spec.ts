import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { cancellationManger } from './cancellation-manger.component';

describe('cancellationManger', () => {
  let component: cancellationManger;
  let fixture: ComponentFixture<cancellationManger>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ cancellationManger ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(cancellationManger);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
