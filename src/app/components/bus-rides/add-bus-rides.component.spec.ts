import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBusRidesComponent } from './add-bus-rides.component';

describe('AddBusRidesComponent', () => {
  let component: AddBusRidesComponent;
  let fixture: ComponentFixture<AddBusRidesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBusRidesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBusRidesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
