import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusRidesComponent } from './bus-rides.component';

describe('BusRidesComponent', () => {
  let component: BusRidesComponent;
  let fixture: ComponentFixture<BusRidesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusRidesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusRidesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
