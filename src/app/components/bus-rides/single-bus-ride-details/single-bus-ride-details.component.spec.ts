import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleBusRideDetailsComponent } from './single-bus-ride-details.component';

describe('SingleBusRideDetailsComponent', () => {
  let component: SingleBusRideDetailsComponent;
  let fixture: ComponentFixture<SingleBusRideDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleBusRideDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleBusRideDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
