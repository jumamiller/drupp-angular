import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StartedRidesComponent } from './started-rides.component';

describe('StartedRidesComponent', () => {
  let component: StartedRidesComponent;
  let fixture: ComponentFixture<StartedRidesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StartedRidesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StartedRidesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
