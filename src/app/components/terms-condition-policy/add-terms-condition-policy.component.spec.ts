import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTermsConditionPolicyComponent } from './add-terms-condition-policy.component';

describe('AddTermsConditionPolicyComponent', () => {
  let component: AddTermsConditionPolicyComponent;
  let fixture: ComponentFixture<AddTermsConditionPolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTermsConditionPolicyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTermsConditionPolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
