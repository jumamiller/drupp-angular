import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StartedRideDetailsComponent } from './started-ride-details.component';

describe('StartedRideDetailsComponent', () => {
  let component: StartedRideDetailsComponent;
  let fixture: ComponentFixture<StartedRideDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StartedRideDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StartedRideDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
