import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchRidesComponent } from './sch-rides.component';

describe('SchRidesComponent', () => {
  let component: SchRidesComponent;
  let fixture: ComponentFixture<SchRidesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchRidesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchRidesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
