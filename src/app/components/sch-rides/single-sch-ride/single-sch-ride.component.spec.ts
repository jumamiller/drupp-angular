import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleSchRideComponent } from './single-sch-ride.component';

describe('SingleSchRideComponent', () => {
  let component: SingleSchRideComponent;
  let fixture: ComponentFixture<SingleSchRideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleSchRideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleSchRideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
