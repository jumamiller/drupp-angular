import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DruppShopAddComponent } from './drupp-shop-add.component';

describe('DruppShopAddComponent', () => {
  let component: DruppShopAddComponent;
  let fixture: ComponentFixture<DruppShopAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DruppShopAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DruppShopAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
