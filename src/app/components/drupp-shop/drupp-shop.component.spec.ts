import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DruppShopComponent } from './drupp-shop.component';

describe('DruppShopComponent', () => {
  let component: DruppShopComponent;
  let fixture: ComponentFixture<DruppShopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DruppShopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DruppShopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
