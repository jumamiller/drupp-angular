import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDruppShopComponent } from './edit-drupp-shop.component';

describe('EditDruppShopComponent', () => {
  let component: EditDruppShopComponent;
  let fixture: ComponentFixture<EditDruppShopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDruppShopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDruppShopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
