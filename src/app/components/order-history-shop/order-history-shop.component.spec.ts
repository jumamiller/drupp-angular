import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderHistoryShopComponent } from './order-history-shop.component';

describe('OrderHistoryShopComponent', () => {
  let component: OrderHistoryShopComponent;
  let fixture: ComponentFixture<OrderHistoryShopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderHistoryShopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderHistoryShopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
