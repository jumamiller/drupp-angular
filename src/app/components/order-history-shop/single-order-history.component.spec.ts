import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleOrderHistoryComponent } from './single-order-history.component';

describe('SingleOrderHistoryComponent', () => {
  let component: SingleOrderHistoryComponent;
  let fixture: ComponentFixture<SingleOrderHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleOrderHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleOrderHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
