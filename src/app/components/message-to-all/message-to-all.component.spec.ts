import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageToAllComponent } from './message-to-all.component';

describe('MessageToAllComponent', () => {
  let component: MessageToAllComponent;
  let fixture: ComponentFixture<MessageToAllComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageToAllComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageToAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
