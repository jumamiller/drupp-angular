import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KekeRidesComponent } from './keke-rides.component';

describe('KekeRidesComponent', () => {
  let component: KekeRidesComponent;
  let fixture: ComponentFixture<KekeRidesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KekeRidesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KekeRidesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
