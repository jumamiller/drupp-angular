import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoolRideComponent } from './pool-ride.component';

describe('PoolRideComponent', () => {
  let component: PoolRideComponent;
  let fixture: ComponentFixture<PoolRideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoolRideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoolRideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
