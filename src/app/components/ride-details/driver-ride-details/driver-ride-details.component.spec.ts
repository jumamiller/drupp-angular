import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverRideDetailsComponent } from './driver-ride-details.component';

describe('DriverRideDetailsComponent', () => {
  let component: DriverRideDetailsComponent;
  let fixture: ComponentFixture<DriverRideDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DriverRideDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverRideDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
