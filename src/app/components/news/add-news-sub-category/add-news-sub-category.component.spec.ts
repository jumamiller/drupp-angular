import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewsSubCategoryComponent } from './add-news-sub-category.component';

describe('AddNewsSubCategoryComponent', () => {
  let component: AddNewsSubCategoryComponent;
  let fixture: ComponentFixture<AddNewsSubCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewsSubCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewsSubCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
