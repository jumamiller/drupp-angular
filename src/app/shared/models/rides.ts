export class Rides {
  base_fare: number;
  cancel_reason: string;
  complete_time: string;
  created_at: string;
  destination: string;
  destination_latitude: string;
  destination_longitude: string;
  distance: number;
  driver_id: number;
  duration: string;
  id: number;
  passengers_preference: string;
  per_km: number;
  per_minute_wait_charge: number;
  requested_at: string;
  ride_date: string;
  ride_option: string;
  ride_time: string;
  ride_type: string;
  rider_id: number;
  source: string;
  source_latitude: string;
  source_longitude: string;
  start_time: string;
  status: number;
  total_fare: number;
  Rider_name: string;
  Driver_name: string;
  updated_at: string;
  user_id: number;
  vehicle_name: string;
  vehicle_number: number;
  vehicle_type: number;
  vehicle_color: string;
  license_number: string;
  vehicle_brand: string;
  vehicle_model: string;
  driver_fare: string;
  vehicle_type_id: number;
  drupp_earnings: number;
}

