export class News {
  news_image: string;
  news_subcategory: string;
  category_name: string;
  headline: string;
  timestamp: string;
  id: number;
}
