export class Vehicles {
  base_fare: number;
  capacity: number;
  icon: string;
  id: number;
  name: string;
  per_minute_wait_charge: number;
  rate_per_km: number;
}
