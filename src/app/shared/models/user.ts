export class User {
  id: number;
  name: string;
  city: string;
  profile_picture: any;
  country_code: number;
  created_at: string;
  email: string;
  first_name: string;
  last_name: string;
  latitude: number;
  longitude: number;
  phone: string;
  average_rating : number;
  // profile_picture: null;
  type: number;
  updated_at: string;
}
