export class Support {
    Rider_name: string;
    created_at: string;
    date: string;
    id: number;
    issue: string;
    phone: string;
    ride_destination: string;
    ride_id: number;
    ride_source: string;
    updated_at: string;
     user_id: number;
}
